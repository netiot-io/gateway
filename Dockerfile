FROM anapsix/alpine-java:8
ADD target/rest-gateway.jar rest-gateway.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /rest-gateway.jar