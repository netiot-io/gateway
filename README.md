# Gateway

Single entrypoint for the netiot backend. It does routing with [zuul](https://github.com/Netflix/zuul)
and service discovery with [zookeeper](https://zookeeper.apache.org/).

## CI/CD
Compile, test, package, build docker image and push to gitlab docker registry.

## Tests
N/A